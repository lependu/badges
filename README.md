### Badges

Static svg badges generated with [![shields.io](https://bitbucket.org/lependu/badges/raw/master/shields.io.svg)](http://shields.io)

For usage see the source of this file
